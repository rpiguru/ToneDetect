import time

from utils.api import get_configuration
from utils.logger import logger


if __name__ == '__main__':

    time.sleep(3)

    while True:
        conf = get_configuration()
        if conf:
            break
        time.sleep(3)

    logger.info(f"Got configuration - {conf}")

    if conf.get('software') == 'tone_alert':
        from bin.tone_detect import start_tone_detector
        start_tone_detector(config=conf)
    elif conf.get('software') == 'statusMonitor':
        from bin.status_monitor import start_status_monitor
        start_status_monitor(config=conf)
    elif conf.get('software') == 'flightTracker':
        from bin.flight_tracker import start_flight_tracker
        start_flight_tracker(config=conf)
    else:
        logger.warning(f"Invalid configuration - {conf}")
