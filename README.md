# ToneDetect

Tone Detecting Program on Raspberry Pi.

## Installation

- Download the Raspbian Buster **Lite** image from [here](http://downloads.raspberrypi.org/raspios_oldstable_lite_armhf/images/raspios_oldstable_lite_armhf-2023-05-03/2023-05-03-raspios-buster-armhf-lite.img.xz) and flash your micro sd card.

- Clone our repository and install

    ```shell script
    cd ~
    sudo apt update
    sudo apt install -y git
    git clone https://gitlab.com/rpiguru/ToneDetect
    cd ToneDetect
    bash setup.sh
    ```

- Wire the components for the StatusMonitor

    ![Breadboard connection](schematic/status_monitor_bb.jpg "Breadboard")

    ![Schematic Connection](schematic/status_monitor_schem.jpg "Schematic")

- Configure the I2C address of the 2nd INA260 chip by following this guide - https://forums.adafruit.com/viewtopic.php?f=19&t=154239

- And reboot!  :)
