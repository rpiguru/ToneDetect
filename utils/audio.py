import pyaudio

from utils.logger import logger


def get_usb_mic_info():
    return get_audio_input(keyword='usb')


def get_audio_input(keyword='default'):
    p = pyaudio.PyAudio()
    for ii in range(p.get_device_count()):
        info = p.get_device_info_by_index(ii)
        if keyword in info.get('name', '').lower() and info.get('maxInputChannels', 0) > 0:
            logger.info(f"Found an audio input named `{keyword}` - {info}")
            return info
