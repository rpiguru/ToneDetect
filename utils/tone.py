from numpy.fft import rfft
import numpy as np
from scipy.signal import hanning


def parabolic(f, x):
    """Quadratic interpolation for estimating the true position of an
    inter-sample maximum when nearby samples are known.
    f is a vector and x is an index for that vector.
    Returns (vx, vy), the coordinates of the vertex of a parabola that goes
    through point x and its two neighbors.
    Example:
    Defining a vector f with a local maximum at index 3 (= 6), find local
    maximum if points 2, 3, and 4 actually defined a parabola.
    In [3]: f = [2, 3, 1, 6, 4, 2, 3, 1]
    In [4]: parabolic(f, argmax(f))
    Out[4]: (3.2142857142857144, 6.1607142857142856)
    """
    xv = 1 / 2. * (f[x - 1] - f[x + 1]) / (f[x - 1] - 2 * f[x] + f[x + 1]) + x
    yv = f[x] - 1 / 4. * (f[x - 1] - f[x + 1]) * (xv - x)
    return xv, yv


def freq_from_fft(sig, fs=44100):
    """
    Estimate frequency from peak of FFT
    """
    # Compute Fourier transform of windowed signal
    windowed = sig * hanning(len(sig))
    f = rfft(windowed)

    # Find the peak and interpolate to get a more accurate peak
    i = np.argmax(abs(f))  # Just use this for less-accurate, naive version
    true_i = parabolic(np.log(abs(f)), i)[0]

    # Convert to equivalent frequency
    return fs * true_i / len(windowed)


if __name__ == '__main__':
    import time
    import soundfile as sf

    filename = "../data/td_samples/810.2-1357.2-Hz.wav"

    print('Reading file "%s"\n' % filename)
    signal, _fs = sf.read(filename, dtype="int16")

    print('Calculating frequency from FFT:', end=' ')
    start_time = time.time()
    print('%f Hz' % freq_from_fft(signal, _fs))
    print('Time elapsed: %.3f s\n' % (time.time() - start_time))
