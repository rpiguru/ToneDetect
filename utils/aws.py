import glob
import ntpath
import os
import time
from datetime import datetime

import boto3

from utils.logger import logger
from utils.settings import LOG_DIR, ALERT_DIR
from utils.common import read_config


def upload_to_aws(local_file, s3_file=None):
    if not s3_file:
        s3_file = ntpath.basename(local_file)
    conf = read_config().get('aws_config', {})
    if conf:
        s3 = boto3.client('s3', aws_access_key_id=conf['access_id'], aws_secret_access_key=conf['secrete'])
        try:
            result = s3.upload_file(local_file, conf['bucket_name'], s3_file)
            logger.info(f"Upload: {local_file}, result: {result}")
            return True
        except Exception as e:
            logger.error(f"Failed to upload - {e}")


def download_from_aws(s3_path, local_path):
    conf = read_config().get('aws_config', {})
    if conf:
        s3 = boto3.client('s3', aws_access_key_id=conf['access_id'], aws_secret_access_key=conf['secrete'])
        try:
            s3.download_file(conf['bucket_name'], s3_path, local_path)
        except Exception as e:
            logger.error(f"Failed to download {s3_path} from AWS to {local_path} - {e}")


def upload_daily_log():

    while True:
        now = datetime.now()
        # Upload at 00:00
        if now.hour == 0 and now.minute == 0:
            logger.info(f">>> Uploading daily log...")
            for log_file in glob.glob(os.path.join(LOG_DIR, '*.*')):
                upload_to_aws(log_file, s3_file=f"Logs/{ntpath.basename(log_file)}")
        time.sleep(60)


def download_tone_alerts(tones):
    for t in tones:
        alert = t.get('detection_tone_alert')
        if alert:
            local_file = os.path.join(ALERT_DIR, f"{alert}.wav")
            if not os.path.isfile(local_file):
                logger.debug(f"Downloading alert file - {local_file}")
                download_from_aws(s3_path=f"Alerts/{alert}.wav", local_path=local_file)


if __name__ == '__main__':

    upload_to_aws(local_file='../setup.sh', s3_file='test/setup.sh')
    # download_from_aws(s3_file='test/test.txt', local_file='downloaded.txt')
