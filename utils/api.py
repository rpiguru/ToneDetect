import math
import os
import platform
import time
import uuid

import requests
import ipinfo

from utils.common import get_serial
from utils.logger import logger
from utils.settings import IPINFO_TOKEN, BASE_URL


def get_configuration():
    try:
        handler = ipinfo.getHandler(access_token=IPINFO_TOKEN)
        details = handler.getDetails()
        payload = {
            "serial_number": get_serial(),
            "ip_address": details.ip,
            "ip_location": details.region,
            "location_lat": details.latitude,
            "location_long": details.longitude,
            "platform": "Raspberry Pi",
            "isp": details.org,
            "city": details.city,
            "country": details.country,
            "postal": details.postal,
            "region": details.region,
            "timezone": details.timezone,
            "os_name": platform.linux_distribution()[0],
            "os_version": open('/etc/debian_version').read(),
            "hardware_version": open('/sys/firmware/devicetree/base/model').read(),
            "ram": math.ceil(int(os.popen('free -m').readlines()[1].split()[1]) / 1024),
            "rom_available": "97",
            "rom_total": "128",
        }
        r = requests.post(url=f"{BASE_URL}/register-connect-hardware", json=payload)
        return r.json()
    except Exception as e:
        logger.error(f"Failed to get configuration - {e}")


def ping_server(conf):
    try:
        payload = dict(
            connect_device_id=conf['connect_device_id'],
            software=conf['software']
        )
        r = requests.post(url=f"{BASE_URL}/connect-hardware-ping", json=payload)
        return r.json()
    except Exception as e:
        logger.error(f"Failed to ping server - {e}")


def upload_detected_tone(conf, tone):
    try:
        payload = dict(
            connect_device_id=conf['connect_device_id'],
            tone_id=tone['tone_id'],
            detected_id=str(uuid.uuid4()),
            time_detected=time.time() * 1000
        )
        r = requests.post(url=f"{BASE_URL}/tone-alert", json=payload)
        logger.debug(f"Tone Detected response({r.status_code}) - `{r.content}`")
        return r.json()
    except Exception as e:
        logger.error(f"Failed to upload detected tone - {e}")


def upload_unknown_tone(conf, tone):
    try:
        tone.update(**dict(
            connect_device_id=conf['connect_device_id'],
            detected_id=str(uuid.uuid4()),
            time_detected=time.time() * 1000
        ))
        r = requests.post(url=f"{BASE_URL}/tone-alert", json=tone)
        logger.debug(f"Tone Detected(unknown) response({r.status_code}) - `{r.content}`")
        return r.json()
    except Exception as e:
        logger.error(f"Failed to upload unknown tone - {e}")


if __name__ == '__main__':

    print(get_configuration())
