import os

_cur_dir = os.path.dirname(os.path.realpath(__file__))

ROOT_DIR = os.path.expanduser('~/.td')
os.makedirs(ROOT_DIR, exist_ok=True)

LOG_DIR = os.path.join(ROOT_DIR, 'logs')
os.makedirs(LOG_DIR, exist_ok=True)

CONFIG_FILE = os.path.join(ROOT_DIR, 'config.json')

AUDIO_DIR = os.path.join(ROOT_DIR, 'audios')
os.makedirs(AUDIO_DIR, exist_ok=True)

ALERT_DIR = os.path.join(ROOT_DIR, 'alerts')
os.makedirs(ALERT_DIR, exist_ok=True)

AIRCRAFT_DIR = os.path.join(ROOT_DIR, 'aircraft')
os.makedirs(AIRCRAFT_DIR, exist_ok=True)

DUMP1090_CMD = f"sudo dump1090 --quiet --write-json {AIRCRAFT_DIR} --net"

CHUNK = 4096

MAX_TONE_LEN = 5
MAX_REC_DURATION = 90

SERVER_HOST = '0.0.0.0'
SERVER_PORT = 80

UPLOAD_TO_AWS = True


PIN_POWER = 21
PIN_INTERNET = 20
PIN_SOCKET_ON = 16
PIN_SOCKET_OFF = 12
PIN_SPEAKER = 26

IPINFO_TOKEN = '52ab137bec4f3c'
BASE_URL = 'https://api.redenes.org/dev/v1'

try:
    from local_settings import *
except ImportError:
    pass
