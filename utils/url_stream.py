from datetime import datetime
import subprocess
import threading
import time

from utils.logger import logger
from utils.settings import CHUNK
from utils.common import kill_process_by_name


CMD = "ffmpeg -i {url} -f wav -ac 1 -ar 44100 pipe:1"


class HttpStreamReader(threading.Thread):

    def __init__(self, url, callback, timeout=1):
        super().__init__()
        self.url = url
        self.timeout = timeout
        self._b_stop = threading.Event()
        self._b_stop.clear()
        self.callback = callback
        self._last_active_time = time.time()
        self._pipe = None
        self._is_streaming = False
        self._tr_monitor = threading.Thread(target=self._check_me, daemon=True)

    def open_stream(self):
        try:
            logger.info(f"Opening online stream - {self.url}")
            self._pipe = subprocess.Popen(CMD.format(url=self.url), shell=True, stdout=subprocess.PIPE)
            return True
        except Exception as e:
            logger.error(f"Failed to open stream - {e}")
            self._pipe = None

    def run(self) -> None:
        self._tr_monitor.start()
        while not self._b_stop.is_set():
            if self._pipe is not None or self.open_stream():
                try:
                    buf = self._pipe.stdout.read(CHUNK)
                    if buf:
                        self._last_active_time = time.time()
                        if callable(self.callback):
                            self.callback(buf)
                        self._is_streaming = True
                    else:
                        self._is_streaming = False
                except Exception as e:
                    logger.exception(f"Failed to read stream and parse - {e}")
                    self._kill_process()
            time.sleep(.001)
        if self._pipe is not None:
            self._pipe = None

    def _check_me(self):
        time.sleep(5)
        while not self._b_stop.is_set():
            if time.time() - self._last_active_time > 3:
                logger.warning(f"URL Stream: No update for 3 sec, killing...")
                self._kill_process()
                break
            print(f"{datetime.now()}:: URL Stream is active.")
            time.sleep(2)

    def _kill_process(self):
        self._is_streaming = False
        kill_process_by_name('ffmpeg')
        self._pipe = None

    def is_streaming(self):
        return self._is_streaming

    def close(self):
        self.stop()
        self._tr_monitor.join(.1)
        self.join(.1)

    def stop(self):
        self._b_stop.set()
