import os
import logging
from logging.handlers import TimedRotatingFileHandler
from utils.settings import LOG_DIR

formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')

handler = TimedRotatingFileHandler(os.path.join(LOG_DIR, 'daily_log.log'), when='midnight', backupCount=2)

handler.setFormatter(formatter)
daily_logger = logging.getLogger('TD')
daily_logger.addHandler(handler)
daily_logger.setLevel(logging.DEBUG)
