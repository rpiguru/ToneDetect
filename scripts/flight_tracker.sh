cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sudo touch /etc/modprobe.d/blacklist-dvb.conf
echo "blacklist dvb_usb_rtl28xxu" | sudo tee -a /etc/modprobe.d/blacklist-dvb.conf

sudo apt install -y cmake build-essential gcc libusb-1.0-0-dev librtlsdr-dev

cd ~
git clone git://git.osmocom.org/rtl-sdr.git
cd rtl-sdr
mkdir build
cd build
cmake ../ -DINSTALL_UDEV_RULES=ON
make -j4
sudo make install
sudo ldconfig
sudo cp ../rtl-sdr.rules /etc/udev/rules.d/

sudo apt install -y libncurses5-dev libncursesw5-dev libbladerf-dev

# https://github.com/on3ure/rpi-dump1090-fa-tar1090
cd ~
wget https://flightaware.com/adsb/piaware/files/packages/pool/piaware/p/piaware-support/piaware-repository_3.8.1_all.deb
sudo dpkg -i piaware-repository_3.8.1_all.deb && rm piaware-repository_3.8.1_all.deb
sudo apt -y update
sudo apt -y install piaware
sudo piaware-config allow-auto-updates yes
sudo piaware-config allow-manual-updates yes
sudo apt -y install dump1090-fa
