"""
    Cythonize the python files to protect the source codes.

    python3 compile.py build_ext --inplace

"""
import glob
import os

from setuptools import setup
from setuptools.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize


def get_folder_list():
    """
        get all files in root_dir directory
    """
    folder_list = []
    for folder, _, files in os.walk('./'):
        for name in files:
            if name.endswith('.py') and folder != './' and name != '__init__.py':
                if folder not in folder_list:
                    folder_list.append(folder)

    return folder_list


if __name__ == '__main__':

    packages = get_folder_list()

    setup(
        name="ToneDetector",
        ext_modules=cythonize(
            [
                Extension(f"{p}.*", [f'{p}/*.py']) for p in packages
            ],
            build_dir="build",
            compiler_directives=dict(
                always_allow_keywords=True,
                language_level="3"
            )
        ),
        cmdclass=dict(
            build_ext=build_ext
        ),
        packages=packages
    )

    print('===== Deleting source files =====')
    for package in packages:
        for path in glob.glob('{}/*.py'.format(package)):
            if '__init__.py' not in path:
                os.remove(path)

    print('Finished!')
