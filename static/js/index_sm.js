
setInterval(check_sm_status, 1000);

function check_sm_status() {
    let xhttp = new XMLHttpRequest();
    xhttp.open("GET", "/sm_status", false);
    xhttp.send();
    let el = document.getElementById("ws_status");
    let resp = JSON.parse(xhttp.response);
    if (resp.socket_status === "ON"){
        el.classList.remove('alert-danger');
        el.classList.add('alert-success');
    } else {
        el.classList.remove('alert-success');
        el.classList.add('alert-danger');
    }
    el.innerHTML = resp.socket_status;

}
