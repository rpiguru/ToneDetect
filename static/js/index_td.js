function updateThreshold(val) {
    document.getElementById('cur_threshold').innerHTML = `${val} dBm`;
    let xhttp = new XMLHttpRequest();
    xhttp.open("GET", "/threshold?threshold=" + val, false);
    xhttp.send();
}

function updateVolumeAdjustment(val) {
    document.getElementById('cur_volumeAdjustment').innerHTML = `${val} dBm`;
    let xhttp = new XMLHttpRequest();
    xhttp.open("GET", "/volumeAdjustment?volumeAdjustment=" + val, false);
    xhttp.send();
}


setInterval(check_td_status, 1000);

function check_td_status() {
    let xhttp = new XMLHttpRequest();
    xhttp.open("GET", "/td_status", false);
    xhttp.send();
    let el = document.getElementById("ws_status");
    let resp = JSON.parse(xhttp.response);
    if (resp.socket_status === "ON"){
        el.classList.remove('alert-danger');
        el.classList.add('alert-success');
    } else {
        el.classList.remove('alert-success');
        el.classList.add('alert-danger');
    }
    el.innerHTML = resp.socket_status;
    let el_stream = document.getElementById("stream_status");
    if (resp.streaming === "ON"){
        el_stream.classList.remove('alert-danger');
        el_stream.classList.add('alert-success');
    } else {
        el_stream.classList.remove('alert-success');
        el_stream.classList.add('alert-danger');
    }
    el_stream.innerHTML = resp.streaming;
    document.getElementById("volume").innerHTML = resp.volume
}
