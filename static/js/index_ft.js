
setInterval(check_ft_status, 1000);

function check_ft_status() {
    let xhttp = new XMLHttpRequest();
    xhttp.open("GET", "/ft_status", false);
    xhttp.send();
    let el = document.getElementById("ws_status");
    let resp = JSON.parse(xhttp.response);
    if (resp.socket_status === "ON"){
        el.classList.remove('alert-danger');
        el.classList.add('alert-success');
    } else {
        el.classList.remove('alert-success');
        el.classList.add('alert-danger');
    }
    el.innerHTML = resp.socket_status;

    let el_sdr = document.getElementById("sdr_status");
    if (resp.sdr_status === "ON"){
        el_sdr.classList.remove('alert-danger');
        el_sdr.classList.add('alert-success');
    } else {
        el_sdr.classList.remove('alert-success');
        el_sdr.classList.add('alert-danger');
    }
    el_sdr.innerHTML = resp.sdr_status;
}
