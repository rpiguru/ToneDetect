cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"
user="$(id -u -n)"

sudo apt update
sudo apt install -y libportaudio0 libportaudio2 libportaudiocpp0 portaudio19-dev libatlas-base-dev ffmpeg

sudo apt install -y python3-pip python3-dev screen

sudo pip3 install -U pip setuptools wheel

sudo pip3 install -r ${cur_dir}/requirements.txt

# Allow port 80 to pi
sudo touch /etc/sysctl.d/50-unprivileged-ports.conf
echo "net.ipv4.ip_unprivileged_port_start=0" | sudo tee -a /etc/sysctl.d/50-unprivileged-ports.conf
sudo sysctl --system


bash ${cur_dir}/scripts/flight_tracker.sh
bash ${cur_dir}/scripts/status_monitor.sh

sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -mS td -d\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -S td -X stuff \"cd ${cur_dir////\\/}\\\\r\"\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -S td -X stuff \"sudo python3 main.py\\\\r\"\'\\nexit 0/g" /etc/rc.local
