import os
from pydub import AudioSegment
import threading
import time
from queue import Queue
import audioop
import wave
import numpy as np
from flask import Flask, render_template, request, redirect, jsonify
import pyaudio

from bin.base import ProgramBase
from utils.api import upload_detected_tone, upload_unknown_tone
from utils.logger import logger
from utils.settings import SERVER_PORT, SERVER_HOST, UPLOAD_TO_AWS, PIN_SPEAKER, ALERT_DIR
from utils.settings import CHUNK, AUDIO_DIR, MAX_TONE_LEN, MAX_REC_DURATION
from utils.aws import upload_to_aws, download_tone_alerts
from utils.common import update_config_file, check_running_proc, is_rpi
from utils.tone import freq_from_fft
from utils.daily_logger import daily_logger

from utils.audio import get_usb_mic_info
from utils.url_stream import HttpStreamReader


if is_rpi:
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(PIN_SPEAKER, GPIO.IN)

audio = pyaudio.PyAudio()

_par_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
template_dir = os.path.join(_par_dir, 'templates')
static_dir = os.path.join(_par_dir, 'static')

app = Flask(__name__, static_url_path='', static_folder=static_dir, template_folder=template_dir)


class ToneDetector(ProgramBase):

    queue = Queue(maxsize=1000)
    play_queue = Queue(maxsize=100)

    def __init__(self):
        super().__init__(sw_type="toneDetect")
        self.sr = 44100     # Sampling rate
        self.stream = None
        self.cur_src = None
        self.volume = 0
        self._last_error_msg_time = 0
        self._connection_status = None
        self._last_detect_time = 0
        self._buf = bytearray()
        self._cur_tone = {}
        self._is_selected_tone = False
        self._audio_process = threading.Thread(target=self.process_audio)
        self._play_stream = None
        self.record_buf = []

    def set_config(self, config):
        threading.Thread(target=download_tone_alerts, daemon=True, args=(config['tone_alert'], )).start()
        super(ToneDetector, self).set_config(config)

    def _create_audio_stream(self):
        if self.cur_src == 'aux_line_in':
            audio_info = get_usb_mic_info()
            if audio_info is not None:
                self.sr = int(audio_info['defaultSampleRate'])
                self.stream = audio.open(format=pyaudio.paInt16, rate=self.sr,
                                         channels=1, input_device_index=audio_info['index'],
                                         input=True, frames_per_buffer=CHUNK, stream_callback=self._on_stream_read)
                self.stream.start_stream()
                return True
            else:
                self.stream = None
        else:
            self.sr = 44100
            url = [s['url'] for s in self.conf.get('streaming', []) if s['name'] == self.cur_src][0]
            logger.info(f">>> Stream URL: {url}")
            self.stream = HttpStreamReader(url=url, callback=self._on_stream_read)
            self.stream.start()
            # Give some delay.
            time.sleep(5)
            return True

    def run(self) -> None:
        threading.Thread(target=self._play_audio).start()
        self._audio_process.start()

        while not self._b_stop.is_set():
            src = self.conf.get('tone_alert_config', {}).get('source', 'aux_line_in')
            if self.cur_src != src:
                logger.info(f">>> Starting new source - {src}")
                if self.stream is not None:
                    self.stream.close()
                    self.stream = None
                self.cur_src = src

            if self.stream is not None or self._create_audio_stream():
                if not self.is_streaming() or (self.cur_src != 'aux_line_in' and not check_running_proc('ffmpeg')):
                    logger.error("Streaming is closed...")
                    self.stream.close()
                    self.stream = None
            else:
                if time.time() - self._last_error_msg_time > 60:
                    self._last_error_msg_time = time.time()
                    logger.error("Failed to start audio stream!")

            if src != 'aux_line_in':
                status = self.is_streaming()
                if self._connection_status != status:
                    logger.info(f"Streaming status changed from {self._connection_status} to {status}")
                    self._connection_status = status
                    # TODO: sent connect/disconnect event
                    payload = {"streamingStatus": [{"source": src, "connection": 'on' if status else 'off'}]}
                    logger.info(f"Connection status changed - {payload}")

            time.sleep(1)

    def _on_stream_read(self, in_data, *args):
        speaker = self.conf['tone_alert_config'].get('speaker_constant_passthrough', True)
        if (is_rpi and GPIO.input(PIN_SPEAKER)) or speaker:
            self.play_queue.put(in_data)
        elif self._cur_tone and time.time() - self._cur_tone['ts'] < self._cur_tone['record_length']:
            self.play_queue.put(in_data)
        self._buf += in_data
        # Each stream packet has 8192 bytes(2 bytes per sample)
        if len(self._buf) >= CHUNK * 2 and not self.queue.full():
            self.queue.put(self._buf[:CHUNK * 2])
            self._buf = self._buf[CHUNK * 2:]
        return in_data, pyaudio.paContinue

    def process_audio(self):
        buf = []
        detected_tones = []
        total = 0
        _tone_cnt = 0
        _last_tone = None
        # s_time = time.time()
        while not self._b_stop.is_set():
            if not self.queue.empty():
                # s_time = time.time()
                while not self.queue.empty():
                    _bytes = self.queue.get()
                    total += len(_bytes) / 2
                    buf.extend(list(np.frombuffer(_bytes, dtype='<i2')))
                    buf = buf[-MAX_TONE_LEN * self.sr:]
                    self.record_buf.append(_bytes)
                    if len(self.record_buf) > MAX_REC_DURATION * self.sr // CHUNK:
                        self.record_buf = self.record_buf[1:]
                # print(datetime.now(), 'total read: ', int(total), (total / (time.time() - s_time)))
                if len(buf) >= MAX_TONE_LEN * self.sr:
                    volume = 20 * np.log10(audioop.rms(b''.join(self.record_buf[-MAX_TONE_LEN * self.sr // CHUNK:]), 2))
                    self.volume = int(volume)
                    if volume > self.conf['tone_alert_config'].get('threshold', 20):
                        # Get unique length group
                        lengths = sorted([(p.get('tone_a_length', 1), p.get('tone_b_length', 3))
                                          for p in self.conf.get('tone_alert', [])], reverse=True)
                        for l_a, l_b in list(set(lengths)):
                            try:
                                a_tone_freq = int(freq_from_fft(buf[-(l_a + l_b) * self.sr:-l_b * self.sr], self.sr))
                                b_tone_freq = int(freq_from_fft(buf[-l_b * self.sr:], self.sr))
                            except Exception as e:
                                logger.error(f"Failed FFT - {e}")
                                continue
                            tolerance = self.conf['tone_alert_config'].get('tolerance', 10)
                            detected = False
                            for t in self.conf.get('tone_alert', []):
                                if int(t.get('tone_a_length', 1)) == l_a and int(t.get('tone_b_length', 3)) == l_b:
                                    if abs(t['tone_a'] - a_tone_freq) < tolerance and \
                                            abs(t['tone_b'] - b_tone_freq) < tolerance and \
                                            time.time() - self._last_detect_time > MAX_TONE_LEN:
                                        detected = True
                                        logger.info(f"Tone Detected! - {t}")
                                        daily_logger.info(f">>> Tone Detected! - {t}")
                                        self._cur_tone = dict(ts=time.time().__int__())
                                        self._cur_tone.update(**t)
                                        detected_tones.append(self._cur_tone)
                                        upload_detected_tone(conf=self.conf, tone=t)
                                        self._last_detect_time = time.time()
                                        self._is_selected_tone = t.get('selected') == 'true'
                            logger.debug(f"Freq: A: {a_tone_freq}, B: {b_tone_freq}, "
                                         f"length: {l_a} & {l_b}, vol: {self.volume}")
                            if not detected and abs(a_tone_freq - b_tone_freq) > 50:
                                tmp = {'tone_a': a_tone_freq, 'tone_b': b_tone_freq,
                                       'tone_a_length': l_a, 'tone_b_length': l_b}
                                if _last_tone is None:
                                    _tone_cnt = 0
                                    _last_tone = tmp
                                elif _last_tone == tmp:
                                    if _tone_cnt < 3:
                                        _tone_cnt += 1
                                    else:
                                        logger.debug(f"Uploading unknown tone - {_last_tone}")
                                        upload_unknown_tone(conf=self.conf, tone=_last_tone)
                                        _last_tone = None
                                        _tone_cnt = 0
                                else:
                                    _tone_cnt = 0
                                    _last_tone = None
                    else:
                        _tone_cnt = 0
                        _last_tone = None
                    #     print(f"Volume: {volume}")

                for i, r in enumerate(detected_tones[:]):
                    if time.time() - r['ts'] - r.get('record_delay', 3) > r['record_length']:
                        detected_tones.pop(i)
                        audio_buf = self.record_buf[:][-int(r['record_length'] * self.sr / CHUNK):]
                        threading.Thread(target=self.save_audio, args=(r, audio_buf, self.sr), daemon=True).start()
                        break
                # print(f"Elapsed: {time.time() - s_time}")

            time.sleep(.01)

    def save_audio(self, info, buf, frame_rate):
        v_adj = self.conf['tone_alert_config'].get('volume_level', 100)
        file_path = os.path.join(AUDIO_DIR, f"{info['detection_tone_alert']}_{info['ts']}.wav")
        logger.info(f"Saving to a file - {file_path}")
        daily_logger.info(f"Saving to a file and uploading - {file_path}")
        wavefile = wave.open(file_path, 'wb')
        wavefile.setnchannels(1)
        wavefile.setsampwidth(audio.get_sample_size(pyaudio.paInt16))
        wavefile.setframerate(frame_rate)
        wavefile.writeframes(b''.join(buf))
        wavefile.close()
        # Adjust volume
        wav = AudioSegment.from_wav(file_path)
        wav.export(file_path, 'wav', parameters=["-ac", "1", "-vol", str(v_adj)])
        if UPLOAD_TO_AWS:
            upload_to_aws(file_path)

    def _play_audio(self):
        while not self._b_stop.is_set():
            if time.time() - self._last_detect_time < .5 and self._is_selected_tone and \
                    self.conf['tone_alert_config'].get('speaker_constant_passthrough', False) == 'selected':
                alert_wav = os.path.join(ALERT_DIR, f"{self._cur_tone.get('detection_tone_alert')}.wav")
                if os.path.isfile(alert_wav) and self._play_stream is not None or self._open_play_stream():
                    logger.info(f"Playing alert file - {alert_wav}")
                    wf = wave.open(alert_wav, 'rb')
                    while True:
                        data = wf.readframes(CHUNK)
                        if data == b'' or time.time() - self._last_detect_time > self._cur_tone.get('record_delay', 3):
                            break
                        self._play_stream.write(data)
                    wf.close()
                    # Pop all streaming audio chunks from queue
                    while not self.play_queue.empty():
                        self.play_queue.get()
            if not self.play_queue.empty():
                buf = self.play_queue.get()
                if self._play_stream is not None or self._open_play_stream():
                    try:
                        self._play_stream.write(buf)
                    except Exception as e:
                        logger.error(f"Failed to play audio - {e}")
                        self._play_stream = None
            else:
                time.sleep(.001)

    def _open_play_stream(self):
        audio_info = get_usb_mic_info()
        if audio_info:
            try:
                self._play_stream = audio.open(format=pyaudio.paInt16, channels=1,
                                               rate=int(audio_info['defaultSampleRate']),
                                               output_device_index=audio_info['index'],
                                               frames_per_buffer=CHUNK,
                                               output=True,
                                               )
                logger.info(f"=== Opened output stream - {audio_info}")
                return True
            except Exception as e:
                logger.error(f"Failed to open the output audio device - {e}")
                self._play_stream = None
                return False
        else:
            self._play_stream = None
            return False

    def set_threshold(self, val):
        self.conf['tone_alert_config']['threshold'] = val
        update_config_file(self.conf)

    def set_volume_adjustment(self, val):
        self.conf['tone_alert_config']['volumeAdjustment'] = val
        update_config_file(self.conf)

    def update_config(self, data):
        self.conf['tone_alert_config'].update(**data)
        update_config_file(self.conf)
        payload = {
            "config": [{
                "threshold": self.conf['tone_alert_config'].get('threshold', 20),
                "volumeAdjustment": self.conf['tone_alert_config'].get('volume_level', 100),
                "source": self.conf['tone_alert_config'].get('source', 'aux_line_in'),
                "speaker_constant_passthrough": self.conf['tone_alert_config'].get('speaker_constant_passthrough', True)
            }]}
        # TODO: Send new configuration to server
        logger.info(f"Configuration updated - {payload}")

    def is_streaming(self):
        if self.cur_src == 'aux_line_in':
            streaming = self.stream is not None and self.stream.is_active
        else:
            streaming = self.stream is not None and self.stream.is_streaming()
        if not streaming:
            self.volume = 'N/A'
        return streaming

    def stop(self):
        self._b_stop.set()
        self.stream.stop_stream()
        self.stream.close()
        audio.terminate()


@app.route('/')
def index():
    threshold = td.conf['tone_alert_config'].get('threshold', 20)
    source = td.conf['tone_alert_config'].get('source', 'aux_line_in')
    volume_adjustment = td.conf['tone_alert_config'].get('volumeAdjustment', 100)
    streams = [s['name'] for s in td.conf.get('streaming', [])]
    speaker = 'On' if td.conf['tone_alert_config'].get('speaker_constant_passthrough') else 'Off'
    return render_template("index_td.html", threshold=threshold, streams=streams, source=source,
                           volumeAdjustment=volume_adjustment, speaker=speaker)


@app.route('/td_status', methods=["GET"])
def get_ws_status():
    return jsonify({
        'volume': str(td.volume),
        'socket_status': "ON" if td.is_connected() else "OFF",
        'streaming': "ON" if td.is_streaming() else 'OFF'
    })


@app.route('/threshold', methods=["GET"])
def update_threshold():
    threshold = int(request.args.get('threshold'))
    td.set_threshold(threshold)
    return "OK", 200


@app.route('/volumeAdjustment', methods=["GET"])
def update_volume_adjustment():
    volume_adjustment = int(request.args.get('volumeAdjustment'))
    td.set_volume_adjustment(volume_adjustment)
    return "OK", 200


@app.route('/config', methods=["POST"])
def config_detector():
    data = request.form.to_dict()
    data['threshold'] = int(data['threshold'])
    td.update_config(data)
    return redirect('/')


td = ToneDetector()


def start_tone_detector(config=None):

    logger.info("========== Starting Tone Detector ==========")
    td.set_config(config)
    td.start()

    try:
        app.run(debug=False, host=SERVER_HOST, port=SERVER_PORT)
    except KeyboardInterrupt:
        td.stop()
