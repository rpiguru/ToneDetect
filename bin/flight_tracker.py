import json
import os
import subprocess
import time
from flask import Flask, render_template, jsonify

from bin.base import ProgramBase
from utils.common import check_running_proc
from utils.logger import logger
from utils.settings import SERVER_HOST, SERVER_PORT, DUMP1090_CMD, AIRCRAFT_DIR

_par_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
template_dir = os.path.join(_par_dir, 'templates')
static_dir = os.path.join(_par_dir, 'static')
app = Flask(__name__, static_url_path='', static_folder=static_dir, template_folder=template_dir)


def _get_aircraft_data():
    try:
        data = json.loads(open(os.path.join(AIRCRAFT_DIR, 'aircraft.json')).read())
        return data
    except Exception as e:
        logger.error(f"Failed to read aircraft data - {e}")
        return {}


class FlightTracker(ProgramBase):

    def __init__(self):
        super().__init__(sw_type="flightTracker")
        self.aircraft_list = []
        self._last_active_time = 0

    def run(self) -> None:
        if not check_running_proc("dump1090"):
            logger.info("Starting dump1090")
            subprocess.Popen(DUMP1090_CMD, shell=True)
        while not self._b_stop.is_set():
            s_time = time.time()
            data = _get_aircraft_data()
            self.aircraft_list = []
            self._last_active_time = data.get("now", 0)
            for air in data.get("aircraft", []):
                if "lat" in air and "lon" in air and data.get("now", 0) > 0:
                    buf = dict(lat=air['lat'], lon=air['lon'], now=data['now'])
                    for k in {"hex", "flight", "alt_baro", "alt_geom", "gs", "track", "squawk", "rssi"}:
                        if k in air:
                            buf[k] = air[k]
                    self.aircraft_list.append(buf)
            if self.aircraft_list:
                logger.debug(f"Uploading aircraft data - {self.aircraft_list}")
                self.send_ws_message({"aircraft": self.aircraft_list})
            elapsed = time.time() - s_time
            if elapsed < 1:
                time.sleep(1 - elapsed)

    def get_sdr_status(self):
        return time.time() - self._last_active_time < 1.5

    def parse_c2d_cmd(self, resp):
        pass


@app.route('/')
def index():
    return render_template("index_ft.html")


@app.route('/ft_status', methods=["GET"])
def get_status():
    return jsonify({
        'socket_status': "ON" if ft.get_ws_status() else "OFF",
        'sdr_status': "ON" if ft.get_sdr_status() else "OFF"
    })


ft = FlightTracker()


def start_flight_tracker(config):

    logger.info("========== Starting Flight Tracker ==========")

    ft.set_config(config)
    ft.start()

    app.run(debug=False, host=SERVER_HOST, port=SERVER_PORT)
