import os
import time
from flask import Flask, render_template, jsonify
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008

from bin.base import ProgramBase
from utils.logger import logger
from utils.settings import SERVER_HOST, SERVER_PORT


_par_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
template_dir = os.path.join(_par_dir, 'templates')
static_dir = os.path.join(_par_dir, 'static')
app = Flask(__name__, static_url_path='', static_folder=static_dir, template_folder=template_dir)


mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(0, 0))


class StatusMonitor(ProgramBase):

    def __init__(self):
        super().__init__(sw_type="statusMonitor")

    def run(self) -> None:
        while not self._b_stop.is_set():
            adc_volts = [round(mcp.read_adc(i) * 3.3 / 1024, 1) for i in range(8)]
            logger.debug(f"ADC Voltages: {adc_volts}")
            ac1 = adc_volts[0]
            ac2 = adc_volts[1]
            # Used voltage divider with 3.3kohm & 220ohm
            dc1 = round(adc_volts[2] * (3300 + 220) / 220, 1)
            dc2 = round(adc_volts[3] * (3300 + 220) / 220, 1)
            logger.debug(f"AC1: {ac1}V, AC2: {ac2}V, DC1: {dc1}V, DC2: {dc2}V")
            time.sleep(1)

    def parse_c2d_cmd(self, resp):
        pass


@app.route('/')
def index():
    return render_template("index_sm.html")


@app.route('/sm_status', methods=["GET"])
def get_status():
    return jsonify({
        'socket_status': "ON" if sm.get_ws_status() else "OFF",
    })


sm = StatusMonitor()


def start_status_monitor(config):
    logger.info("========== Starting Status Monitor ==========")
    sm.set_config(config)
    sm.start()

    app.run(debug=False, host=SERVER_HOST, port=SERVER_PORT)
