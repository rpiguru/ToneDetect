import multiprocessing
import threading
import time

from utils.aws import upload_daily_log
from utils.common import update_config_file, is_rpi, check_internet_connection
from utils.logger import logger
from utils.api import ping_server
from utils.settings import PIN_SOCKET_ON, PIN_SOCKET_OFF, PIN_INTERNET, PIN_POWER


if is_rpi:
    import RPi.GPIO as GPIO
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(PIN_INTERNET, GPIO.OUT)
    GPIO.setup(PIN_SOCKET_ON, GPIO.OUT)
    GPIO.setup(PIN_SOCKET_OFF, GPIO.OUT)
    GPIO.setup(PIN_POWER, GPIO.OUT)


class ProgramBase(threading.Thread):

    def __init__(self, sw_type=None):
        super().__init__(daemon=True)
        self._b_stop = threading.Event()
        self._b_stop.clear()
        self._is_ws_connected = threading.Event()
        self.conf = {}
        self.sw_type = sw_type
        multiprocessing.Process(target=process_internet, daemon=True).start()
        multiprocessing.Process(target=upload_daily_log, daemon=True).start()
        GPIO.output(PIN_POWER, True)
        self._last_ping_ts = 0

    def set_config(self, config):
        self.conf = config
        update_config_file(config)
        threading.Thread(target=self._ping_server).start()

    def _ping_server(self):
        last_ts = 0
        while not self._b_stop.is_set():
            if time.time() - last_ts >= self.conf.get('ping_interval', 300):
                r = ping_server(conf=self.conf)
                if r is not None:
                    self._last_ping_ts = time.time()
                    last_ts = time.time()
                else:
                    time.sleep(5)
            time.sleep(1)

    def is_connected(self):
        return time.time() - self._last_ping_ts < 300


def process_internet():
    has_internet = False
    while True:
        if check_internet_connection():
            if not has_internet:
                logger.info(f"Connected to the internet...")
                has_internet = True
        else:
            if has_internet:
                logger.warning(f"Disconnected to the internet...")
                has_internet = False
        if is_rpi:
            GPIO.output(PIN_INTERNET, has_internet)
        time.sleep(1)
